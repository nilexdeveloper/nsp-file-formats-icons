module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            options: {
                sourceMap: true,
                outputStyle: "nested" // TODO: "compresed" doesn't work in Safari
            },
            dist: {
                files: {
                    'dist/css/nsp-file-formats-icons.css': 'src/nsp-file-formats-icons.scss',
                    'examples/nsp-file-formats-icons-demo.css': 'examples/nsp-file-formats-icons-demo.scss'             
                }
            }
        },

        copy: {
            main:{
                files: [
                    {
                        expand: true,
                        src: '**',
                        dest: 'dist/fonts',
                        cwd: 'src/fonts'            
                    }
                ]
            }
        },

        concat: {

            options: {
                process: function(src, filepath) {
                    return '\n/****************************************\n    FILE: ' + filepath + '\n****************************************/\n\n' + src;
                }
            },

        },

        wiredep: {
            task: {
                src: ['index.html']
            },
            options: {
                devDependencies: true
            }
        },

        jshint: {
            beforeconcat: [
                'Gruntfile.js',
                'src/*.js'
            ],
            options: {
                notypeof: true,
                debug: true,
                eqnull: true,
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }

        },

        watch: {
            all: {
                files: ['Gruntfile.js',
                    '**/*.scss',
                    'src/components/**/*.html'
                ],
                tasks: ['default'],
                options: {
                    atBegin: true,
                    livereload: true
                }
            }
        },

        connect: {
            server: {
                options: {
                    livereload: false,
                    keepalive: true,
                    port: 8000,
                    hostname: '*'
                }
            }
        },

        clean: {
            dist: {
                src: ["dist/**/*"],
                filter: function(filepath) {
                    if (!grunt.file.isDir(filepath)) {
                        return true;
                    }
                    return (fs.readdirSync(filepath).length === 0);
                }
            }
        }

    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', ['clean', 'concat', 'sass', 'newer:jshint', 'wiredep', 'copy:main']);

};