(function() {
    'use strict';

    angular
        .module('NSPFileFormatsIcons')
        .controller('NSPFileFormatsIconsController', NSPFileFormatsIconsController);

    NSPFileFormatsIconsController.$inject = ['$scope', '$http'];

    /* @ngInject */
    function NSPFileFormatsIconsController($scope, $http) {
        var vm = this;
        vm.title = 'FontIcons';
        vm.icons = [];
        $http.get('examples/classes-model.json').success(function(data) {
           vm.icons = data;
        });
    }
})();